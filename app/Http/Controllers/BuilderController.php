<?php

namespace App\Http\Controllers;

use App\Models\FormBuilder;
use App\Models\FormSubmission;
use Illuminate\Http\Request;

class BuilderController extends Controller
{
    // public function index()
    // {
    //     return view('builder')->with([
    //         'definition' => null
    //     ]);
    // }
    

    // public function store(Request $request)
    // {
    //     $data = $request->all();
    //     FormBuilder::create(['form' => json_encode($data)]);
    // }

    // public function renderForm()
    // {
    //     $form = FormBuilder::where('id', 1)->first();
    //     return view('form', ['form' => json_decode($form->form)]);
    // }

    public function create()
    {
        return view('builder')->with([
            // can provide a previously-saved form definition here
            'definition' => null, 
        ]);
    }
    
    public function store(Request $request)
    {
        
        $data = $request->validate([
            'definition' => 'required|json',
            'form_name' => 'required|string'
        ]);
        // dd($data);
        FormBuilder::create([
            'form' => $data['definition'],
            'form_name' => $data['form_name']
        ]);
        
    }

    public function renderForm()
    {
        $formBuilder = FormBuilder::findOrFail(1);
        $definition = json_decode($formBuilder->form);
        return view('renderForm')->with([
            'definition' => $definition,
            'form_builder_id' => $formBuilder->id,
            'data' => '{}'
        ]);
    }

    public function formStore(Request $request)
    {
        FormSubmission::create([
            'form_builder_id' => $request->form_builder_id,
            'submission' => $request->submissionValues
        ]);
    }

}
